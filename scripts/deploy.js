const fs = require('fs-extra');
const path = require('path');

/*********** Settings **************************************/
const projectName = 'pwa-light';
const servers = {
	preprod: '192.168.245.11',
	serjs1: '192.168.245.12',
	serjs2: '192.168.245.13',
};
const serverDeployPath = 'c$/Outils/Sites/pwa';
const nbBuildsKeep = 3;

/*********** Helpers **************************************/

const readJson = (path, success, error) => {
	fs.readFile(require.resolve(path), (err, data) => {
		if (err) {
			error(err);
		} else {
			success(JSON.parse(data));
		}
	})
}

const colors = {
	info: '\x1b[33m',
	error: '\x1b[31m',
	success: '\x1b[32m',
	reset: '\x1b[0m'
};

const color = (message, color, previous) => {
	return `${color}${message}${previous}`;
}

const displayError = (message) => {
	console.error(color(message, colors.error, colors.reset));
}

const displaySuccess = (message) => {
	console.log(color(message, colors.success, colors.reset));
}

const displayInfo = (message) => {
	console.log(color(message, colors.info, colors.reset));
}

/*********** Commands line parameters *********************/

const configuration = process.argv.slice(2)[0];
if (!configuration) {
	displayError('Error: The first parameter must be the configuration name (See the angular.json file)\n');
	process.exit(1);
}

const serverIp = servers[process.argv.slice(2)[1]];
if (!serverIp) {
	displayError(`Error: The second parameter must be the server to deploy (${Object.keys(servers).join('|')})\n`);
	process.exit(1);
}

/*********** RUN **************************************/

readJson('../angular.json', (angular) => {
	const distToCopy = angular.projects[projectName].architect.build.configurations[configuration].outputPath;
	const dirName = path.basename(distToCopy);
	const deployPath = `//${serverIp}/${serverDeployPath}`;
	const versionConfigFolderPath = `${deployPath}/versions/${configuration}`;

	displayInfo(`Ensure "${color(versionConfigFolderPath, colors.reset, colors.info)}" exists.`);
	fs.ensureDirSync(versionConfigFolderPath);
	// Create the version folder of the build and overrite the current build.
	const time = Date.now();
	const versionConfigPath = `${versionConfigFolderPath}/${dirName + '_deploy_build_' + time}`;

	displayInfo(`Copy the build from "${color('./' + distToCopy, colors.reset, colors.info)}" to "${color(versionConfigPath, colors.reset, colors.info)}".`);
	fs.copySync(`./${distToCopy}`, versionConfigPath);

	displayInfo('Overrite the current build.');
	fs.emptyDirSync(`${deployPath}/${dirName}`);
	fs.copySync(versionConfigPath, `${deployPath}/${dirName}`);

	// Remove old builds
	const versions = fs.readdirSync(versionConfigFolderPath);
	versions.sort((a, b) => {
		const aTime = parseInt(a.split('_deploy_build_')[1]), bTime = parseInt(b.split('_deploy_build_')[1]);
		return aTime - bTime;
	});
	versions.reverse();
	const versionsToDelete = versions.slice(nbBuildsKeep);
	versionsToDelete.forEach((versions) => {
		fs.removeSync(`${versionConfigFolderPath}/${versions}`);
	});
	displayInfo(`Remove ${color(versionsToDelete.length, colors.reset, colors.info)} old builds.`);
	displaySuccess('Successfully deployed.');
}, (error) => displayError(error));
