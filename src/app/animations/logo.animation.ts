import { keyframes, style, query, animate } from '@angular/animations';

export const logoEnterAnim = keyframes([
	style({ transform: 'scale3d(1, 1, 1)', opacity: 0, offset: 0 }),
	style({ transform: 'scale3d(1.25, 0.75, 1)', opacity: 1, offset: 0.3 }),
	style({ transform: 'scale3d(0.75, 1.25, 1)', offset: 0.4 }),
	style({ transform: 'scale3d(1.15, 0.85, 1)', offset: 0.5 }),
	style({ transform: 'scale3d(.95, 1.05, 1)', offset: 0.65 }),
	style({ transform: 'scale3d(1.05, .95, 1)', offset: 0.75 }),
	style({ transform: 'scale3d(1, 1, 1)', offset: 1 }),
]);

export function logoEnterAnimation(selector = '.logo-container', timings: string | number = 500) {
	return query(selector, [
		animate(timings, logoEnterAnim)
	]);
}

export function logoLeaveAnimation(selector = '.logo-container', timings: string | number = 500) {
	return query(selector, [
		style({ opacity: 1 }),
		animate(timings, style({ opacity: 0 }))
	]);
}
