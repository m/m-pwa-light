import { style, query, animateChild, group, animate, stagger } from '@angular/animations';

export const pageFadeIn = [
	style({ position: 'relative' }),
	query(':enter, :leave', [
		style({
			position: 'fixed', // required to play animation
			left: 0, // The left property is required to play the animation, do not set the top property.
			width: '100%',
		})
	], { optional: true }),
	query(':enter .inner-content', [
		style({ opacity: 0 })
	], { optional: true }),
	query(':enter .main-toolbar', [
		style({ paddingTop: '42px' })
	], { optional: true }),
	query(':enter .enter-fade', [
		style({ opacity: 0 })
	], { optional: true }),
	query(':leave', animateChild(), { optional: true }),
	group([
		// Fix "Child routes disappear immediately instead of playing :leave animation" https://github.com/angular/angular/issues/15477
		query('router-outlet ~ *', [style({}), animate(1, style({}))], { optional: true }),
		query(':leave', [
			animate('220ms ease-in', style({ opacity: 0, transform: 'translate(0, -5%)' }))
		], { optional: true }),
		query(':enter .inner-content', [
			animate('220ms ease-in', style({ opacity: 1 }))
		], { optional: true }),
		query(':enter .main-toolbar', [
			animate('220ms 110ms ease-in', style({ paddingTop: '*' }))
		], { optional: true }),
		query(':enter .enter-fade', [
			stagger(90, animate('220ms 190ms ease-in', style({ opacity: 1 })))
		], { optional: true })
	]),
	query(':enter', animateChild(), { optional: true }),
];

export const pageFadeOut = [
	style({ position: 'relative' }),
	query(':enter, :leave', [
		style({
			position: 'fixed', // required to play animation
			left: 0, // The left property is required to play the animation, do not set the top property.
			width: '100%',
		})
	], { optional: true }),
	query(':enter', [
		style({ opacity: 0, transform: 'translate(0, -5%)' })
	], { optional: true }),
	query(':leave', animateChild(), { optional: true }),
	group([
		// Fix "Child routes disappear immediately instead of playing :leave animation" https://github.com/angular/angular/issues/15477
		query('router-outlet ~ *', [style({}), animate(1, style({}))], { optional: true }),
		query(':leave', [
			animate('220ms ease-out', style({ opacity: 0, transform: 'translate(0, 5%)' }))
		], { optional: true }),
		query(':enter', [
			animate('220ms 110ms ease-out', style({ opacity: 1, transform: 'translate(0, 0)' }))
		], { optional: true })
	]),
	query(':enter', animateChild(), { optional: true }),
];
