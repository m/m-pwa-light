import { style, query, animateChild, group, animate } from '@angular/animations';

export const slideRight = [
	style({ position: 'relative' }),
	query(':enter, :leave', [
		style({
			position: 'fixed', // required to play animation
			left: 0, // The left property is required to play the animation, do not set the top property.
			width: '100%',
			maxWidth: '768px',
		})
	], { optional: true }),
	query(':enter', [
		style({ left: '100%' })
	], { optional: true }),
	query(':leave', animateChild(), { optional: true }),
	group([
		// Fix "Child routes disappear immediately instead of playing :leave animation" https://github.com/angular/angular/issues/15477
		query('router-outlet ~ *', [style({}), animate(1, style({}))], { optional: true }),
		query(':leave', [
			animate('330ms ease-in', style({ left: '-100%' }))
		], { optional: true }),
		query(':enter', [
			animate('330ms ease-in', style({ left: '0%' }))
		], { optional: true }),
	]),
	query(':enter', animateChild(), { optional: true }),
];
