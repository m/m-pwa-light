import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LineDetailComponent } from '@pages/line-detail/line-detail.component';
import { StopsZoneComponent } from '@pages/stops-zone/stops-zone.component';
import { BrowserComponent } from '@pages/browser/browser.component';
import { LineBrowserComponent } from '@pages/browser/children/line-browser/line-browser.component';
import { MapBrowserComponent } from '@pages/browser/children/map-browser/map-browser.component';
import { HomeComponent } from '@pages/home/home.component';
import { LinesResolver, MapsResolver, ClustersResolver } from '@metromobilite/m-features/core';
import { NearestStopsResolver } from '@metromobilite/m-features/nearest-stops';
import { PoiResolver } from '@metromobilite/m-features/reference';


const routes: Routes = [
	{
		path: '', component: HomeComponent,
		resolve: {
			lines: LinesResolver,
			nearestClusters: NearestStopsResolver,
		},
		data: {
			animation: 'home',
			title: 'M - Lite'
		}
	},
	{
		path: 'parcourir', component: BrowserComponent,
		resolve: {
			lines: LinesResolver,
		},
		children: [
			{ path: '', redirectTo: 'lignes', pathMatch: 'full' },
			{
				path: 'lignes', component: LineBrowserComponent,
				data: {
					animation: 'linebrowser',
					title: 'M - Trouver une ligne ou un arrêt'
				},
			},
			{
				path: 'plans', component: MapBrowserComponent,
				resolve: {
					maps: MapsResolver
				},
				data: {
					animation: 'mapbrowser',
					title: 'M - Trouver un plan'
				},
			},
		]
	},
	{
		path: 'parcourir/ligne/:id', component: LineDetailComponent,
		resolve: {
			clusters: ClustersResolver
		},
		data: {
			animation: 'line'
		}
	},
	{
		path: 'parcourir/lignes/zone-arret/:id', component: StopsZoneComponent,
		resolve: {
			poi: PoiResolver,
			lines: LinesResolver,
		},
		data: {
			animation: 'poi'
		}
	},
	{
		path: 'zone-arret/:id', component: StopsZoneComponent,
		resolve: {
			poi: PoiResolver,
			lines: LinesResolver,
		},
		data: {
			animation: 'poi'
		}
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes, { enableTracing: false, relativeLinkResolution: 'legacy' })],
	exports: [RouterModule]
})
export class AppRoutingModule { }
