import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, NavigationEnd, RouterOutlet, NavigationStart, ActivatedRoute } from '@angular/router';
import { trigger, transition, style, animate, query, stagger } from '@angular/animations';
import { Title } from '@angular/platform-browser';
import { takeUntil, filter, tap, map, mergeMap } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { MatSidenav } from '@angular/material/sidenav';

import { SidenavService } from '@features/sidenav/sidenav.service';
import { UpdateService } from '@features/update/update.service';
import { slideRight } from '@animations/slide-right.animation';
import { slideLeft } from '@animations/slide-left.animation';
import { logoEnterAnimation, logoLeaveAnimation } from '@animations/logo.animation';
import { pageFadeIn, pageFadeOut } from '@animations/page-fade.animation';
import { UserSettingsService, StatusBarService, THEMES } from '@metromobilite/m-features/core';
import { MatomoInjector, MatomoTracker } from 'ngx-matomo-v9';
import { environment } from 'src/environments/environment';
import { FavoritesService } from '@metromobilite/m-features/favorites';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
	animations: [
		trigger('routeAnimations', [
			transition('void => *', pageFadeIn),
			transition('* => line', pageFadeIn),
			transition('* => poi', pageFadeIn),
			transition('line => linebrowser, line => mapbrowser', pageFadeOut),
			transition('poi => home, poi => linebrowser, poi => mapbrowser', pageFadeOut),
			transition('home => *', slideRight),
			transition('* => favorites, * => home', slideLeft),
		]),
		trigger('appLoaderAnimation', [
			transition(':enter', [
				logoEnterAnimation()
			]),
			transition(':leave', [
				logoLeaveAnimation()
			])
		]),
		trigger('navBarAnimation', [
			transition(':enter', [
				query('a', style({ opacity: 0, transform: 'translate(0, 20%)' })),
				style({ opacity: 0, transform: 'translate(0, 50%)' }),
				animate('330ms 500ms ease-in', style({ opacity: 1, transform: 'translate(0, 0)' })),
				query('a', stagger(150, animate(330, style({ opacity: 1, transform: 'translate(0, 0)' }))))
			])
		]),
	]
})
export class AppComponent implements OnInit, OnDestroy {

	@ViewChild(MatSidenav, { static: true }) sidenav: MatSidenav;

	displayLoader: boolean;
	defaultPage: string;

	private unsubscriber = new Subject<void>();
	private isHome = true;

	constructor(
		private userSettings: UserSettingsService,
		private statusBarService: StatusBarService,
		private router: Router,
		private sidenavService: SidenavService,
		private updateService: UpdateService,
		private title: Title,
		private activatedRoute: ActivatedRoute,
		private matomoInjector: MatomoInjector,
		private matomoTracker: MatomoTracker
	) {
		if (environment.matomo) {
			this.matomoInjector.init(environment.matomoConfig.url, environment.matomoConfig.id);
		}
	}

	ngOnInit(): void {
		this.sidenavService.registerSidenav(this.sidenav);
		let shouldDisplayLoaderTimer: any;
		// Route transitions
		this.router.events.pipe(
			tap((event) => {
				if (event instanceof NavigationStart) {
					if (shouldDisplayLoaderTimer) {
						clearTimeout(shouldDisplayLoaderTimer);
					}
					shouldDisplayLoaderTimer = setTimeout(() => {
						this.displayLoader = true;
					}, 1000);
				}
				if (event instanceof NavigationEnd) {
					this.isHome = event.url === '/';
				}
			}),
			filter(event => event instanceof NavigationEnd),
			map(() => this.activatedRoute),
			map(route => {
				while (route.firstChild) route = route.firstChild;
				return route;
			}),
			mergeMap(route => route.data),
			takeUntil(this.unsubscriber)
		).subscribe((data) => {
			this.title.setTitle(data.title || 'M - Light');
			if (shouldDisplayLoaderTimer) {
				clearTimeout(shouldDisplayLoaderTimer);
			}
			this.displayLoader = false;
			this.statusBarService.updateStatusBar(this.getStatusBarColor());
			if (environment.matomoConfig.events.navigation) {
				this.matomoTracker.trackEvent('navigation', this.router.url);
			}
		});

		this.statusBarService.updateStatusBar(this.getStatusBarColor());
		this.userSettings.theme$.pipe(takeUntil(this.unsubscriber)).subscribe(() => {
			this.statusBarService.updateStatusBar(this.getStatusBarColor());
		});
	}

	ngOnDestroy(): void {
		this.unsubscriber.next();
		this.unsubscriber.complete();
	}

	prepareRoute(outlet: RouterOutlet) {
		let animation: string;
		if (outlet) {
			if (outlet.isActivated && outlet.activatedRoute.firstChild) {
				animation = outlet.activatedRoute.firstChild.snapshot.data.animation;
			} else {
				animation = outlet.activatedRouteData && outlet.activatedRouteData.animation;
			}
		}
		return animation || 'void';
	}

	private getStatusBarColor() {
		return this.userSettings.theme === THEMES.DARK ? (this.isHome ? '#16111a' : '#39353c') : (this.isHome ? '#eeeeee' : '#ffffff');
	}
}
