import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTabsModule } from '@angular/material/tabs';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatListModule } from '@angular/material/list';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatMenuModule } from '@angular/material/menu';
import { MatRippleModule } from '@angular/material/core';

import { DragDropModule } from '@angular/cdk/drag-drop';

import { MFeaturesModule } from '@metromobilite/m-features';
import { RealtimeDataModule, STRATEGY_CHRONO_OPTIONS } from '@metromobilite/m-features/realtime-data';
import { TimeSheetModule } from '@metromobilite/m-features/time-sheet';
import { SearchModule } from '@metromobilite/m-features/search';
import { NearestStopsModule } from '@metromobilite/m-features/nearest-stops';
import { FavoritesModule } from '@metromobilite/m-features/favorites';
import { ReferenceModule } from '@metromobilite/m-features/reference';
import { CoreModule } from '@metromobilite/m-features/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { environment } from '../environments/environment';
import { domain } from '@helpers/domain.helpers';
import { LineDetailComponent } from '@pages/line-detail/line-detail.component';
import { StopsZoneComponent } from '@pages/stops-zone/stops-zone.component';
import { BrowserComponent } from '@pages/browser/browser.component';
import { LineBrowserComponent } from '@pages/browser/children/line-browser/line-browser.component';
import { MapBrowserComponent } from '@pages/browser/children/map-browser/map-browser.component';
import { HomeComponent } from '@pages/home/home.component';
import { SidenavDirective } from '@features/sidenav/sidenav.directive';
import { BackDirective } from '@features/back/back.directive';
import { SearchFavoritesDialogComponent } from '@features/favorites/search-favorites-dialog/search-favorites-dialog.component';
import { ErrorToastComponent } from './components/error-toast/error-toast.component';
import { ErrorInterceptor } from './interceptors/error.interceptor';
import { MatomoModule } from 'ngx-matomo-v9';
import { MatomoTrackEventDirective } from '@features/matomo/matomo-track-event.directive';

@NgModule({
	declarations: [
		AppComponent,
		LineDetailComponent,
		StopsZoneComponent,
		BrowserComponent,
		SidenavDirective,
		LineBrowserComponent,
		MapBrowserComponent,
		HomeComponent,
		BackDirective,
		SearchFavoritesDialogComponent,
		ErrorToastComponent,
		MatomoTrackEventDirective,
	],
	imports: [
		BrowserAnimationsModule,
		BrowserModule,
		HttpClientModule,
		FormsModule,
		ReactiveFormsModule,
		AppRoutingModule,
		ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
		MatSidenavModule,
		MatIconModule,
		MatButtonModule,
		MatRippleModule,
		MatFormFieldModule,
		MatSelectModule,
		MatSnackBarModule,
		MatTabsModule,
		MatProgressSpinnerModule,
		MatListModule,
		MatDialogModule,
		MatInputModule,
		MatCheckboxModule,
		MatCardModule,
		MatExpansionModule,
		MatProgressBarModule,
		MatMenuModule,
		DragDropModule,
		MFeaturesModule.forRoot({
			domain
		}),
		CoreModule,
		RealtimeDataModule,
		TimeSheetModule,
		SearchModule,
		NearestStopsModule,
		FavoritesModule,
		ReferenceModule,
		MatomoModule,
	],
	exports: [
		SearchFavoritesDialogComponent,
		ErrorToastComponent,
	],
	providers: [
		// Override chrono options at the rool level.
		{ provide: STRATEGY_CHRONO_OPTIONS, useValue: { maximumPresenceLine: 2 } },
		// HTTP interceptors
		{ provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
