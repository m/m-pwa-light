import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
	selector: 'app-error-toast',
	templateUrl: './error-toast.component.html',
	styleUrls: ['./error-toast.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ErrorToastComponent implements OnInit {

	constructor() { }

	ngOnInit(): void {
	}

	tryAgain() {
		location.reload();
	}

}
