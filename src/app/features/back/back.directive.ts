import { Directive, HostListener, Input } from '@angular/core';
import { Router } from '@angular/router';

@Directive({
	selector: '[appBack]'
})
export class BackDirective {

	@Input() url: any[];

	constructor(private router: Router) { }

	@HostListener('click', ['$event']) onClick(event: MouseEvent) {
		if (this.url) {
			this.router.navigate(this.url);
		} else {
			history.back();
		}
		event.preventDefault();
		event.stopPropagation();
	}

}
