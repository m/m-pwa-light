import { Component, OnInit } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';
import { MatDialogRef } from '@angular/material/dialog';
import { FavoritesService, FavoriteLinesSelectorDialogService } from '@metromobilite/m-features/favorites';
import { Poi } from '@metromobilite/m-features/reference';

@Component({
	templateUrl: './search-favorites-dialog.component.html',
	styleUrls: ['./search-favorites-dialog.component.scss'],
	animations: [
		trigger('fade', [
			transition(':enter', [
				style({ opacity: 0 }),
				animate('250ms', style({ opacity: 1 }))
			]),
			transition(':leave', [
				style({ opacity: 1 }),
				animate('250ms', style({ opacity: 0 }))
			]),
		]),
	]
})
export class SearchFavoritesDialogComponent implements OnInit {

	constructor(
		private dialogRef: MatDialogRef<any>,
		private favoritesService: FavoritesService,
		private favoriteLinesSelectorDialogService: FavoriteLinesSelectorDialogService
	) { }

	ngOnInit(): void {
	}

	close(item: Poi) {
		this.favoriteLinesSelectorDialogService.open(item).subscribe(response => {
			this.favoritesService.handleLineSelectorResponse(response, item);
			this.dialogRef.close();
		});
	}

}
