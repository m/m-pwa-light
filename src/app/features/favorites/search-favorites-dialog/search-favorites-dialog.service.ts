import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { SearchFavoritesDialogComponent } from './search-favorites-dialog.component';

@Injectable({
	providedIn: 'root'
})
export class SearchFavoritesDialogService {

	constructor(private dialog: MatDialog) { }

	open<T>(): Observable<T> {
		return this.dialog.open<SearchFavoritesDialogComponent, any, T>(SearchFavoritesDialogComponent, {
			panelClass: ['m-theme', 'search-point'],
			position: {
				left: '0',
			},
		}).afterClosed();
	}

}
