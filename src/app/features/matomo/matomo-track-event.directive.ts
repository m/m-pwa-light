import { Directive, Input, HostListener } from '@angular/core';
import { MatomoTracker } from 'ngx-matomo-v9';
import { environment } from 'src/environments/environment';

@Directive({
	selector: '[appMatomoTrackEvent]'
})
export class MatomoTrackEventDirective {

	@Input('appMatomoTrackEvent') eventCategory: string;
	@Input() eventAction: string;

	constructor(private matomoTracker: MatomoTracker) { }

	@HostListener('click')
	track() {
		if (environment.matomoConfig.events[this.eventCategory]) {
			this.matomoTracker.trackEvent(this.eventCategory, this.eventAction);
		}
	}

}
