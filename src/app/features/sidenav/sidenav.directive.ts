import { Directive, Input, HostListener } from '@angular/core';
import { SidenavService } from './sidenav.service';

@Directive({
	selector: '[appSidenav]'
})
export class SidenavDirective {

	@Input('appSidenav') action: 'open' | 'close' | 'toggle';

	constructor(private sidenavService: SidenavService) { }

	@HostListener('click') onClick() {
		this.sidenavService[this.action]();
	}

}
