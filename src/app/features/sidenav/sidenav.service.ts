import { Injectable } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';

@Injectable({
	providedIn: 'root'
})
export class SidenavService {

	private _sidenav: MatSidenav;

	constructor() { }

	registerSidenav(sidenav: MatSidenav) {
		this._sidenav = sidenav;
	}

	get sidenav(): MatSidenav {
		return this._sidenav;
	}

	open() {
		if (this.sidenav) {
			this.sidenav.open();
		}
	}

	close() {
		if (this.sidenav) {
			this.sidenav.close();
		}
	}

	toggle() {
		if (this.sidenav) {
			this.sidenav.toggle();
		}
	}

}
