import { Injectable } from '@angular/core';
import {
	HttpRequest,
	HttpHandler,
	HttpEvent,
	HttpInterceptor
} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { catchError } from 'rxjs/operators';
import { ErrorToastComponent } from '@components/error-toast/error-toast.component';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

	constructor(private snackBar: MatSnackBar) { }

	intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
		return next.handle(request).pipe(
			catchError(error => {
				this.snackBar.openFromComponent(ErrorToastComponent);
				return of(error);
			})
		);
	}
}
