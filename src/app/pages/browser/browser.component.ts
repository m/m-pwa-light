import { Component, OnInit } from '@angular/core';
import { trigger, transition } from '@angular/animations';
import { slideRight } from '@animations/slide-right.animation';
import { slideLeft } from '@animations/slide-left.animation';
import { RouterOutlet } from '@angular/router';

@Component({
	templateUrl: './browser.component.html',
	styleUrls: ['./browser.component.scss'],
	animations: [
		trigger('childrenRouteAnimations', [
			transition('linebrowser => mapbrowser', slideRight),
			transition('mapbrowser => linebrowser', slideLeft),
		]),
	]
})
export class BrowserComponent implements OnInit {

	navLinks = [
		{ path: '/parcourir/lignes', label: 'Lignes' },
		{ path: '/parcourir/plans', label: 'Plans' },
	];

	constructor() { }

	ngOnInit(): void {
	}

	prepareRoute(outlet: RouterOutlet) {
		return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation;
	}

}
