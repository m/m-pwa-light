import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { animate, query, stagger, style, transition, trigger } from '@angular/animations';
import { Line, AVAILABLE_LINE_TYPES } from '@metromobilite/m-features/core';

@Component({
	templateUrl: './line-browser.component.html',
	styleUrls: ['./line-browser.component.scss'],
	animations: [
		trigger('after', [
			transition('* <=> *', [
				query(':enter .mat-card-title, :enter .mat-card-subtitle, :enter .mat-card-content', [
					style({ opacity: 0 }),
					stagger(50, animate(150, style({ opacity: 1 })))
				], { optional: true })
			]),
		]),
	]
})
export class LineBrowserComponent implements OnInit {
	lines: Line[] = [];
	ghosts = Array(4);
	fakeLoading = true;
	triggerAnimation = true;

	constructor(
		private activatedRoute: ActivatedRoute,
		@Inject(AVAILABLE_LINE_TYPES) public availableLineTypes: string[]
	) { }

	ngOnInit(): void {
		setTimeout(() => {
			this.lines = this.activatedRoute.snapshot.data.lines || this.activatedRoute.snapshot.parent.data.lines;
			this.lines = this.lines.filter(line => this.availableLineTypes.includes(line.type));
			this.fakeLoading = false;
			this.triggerAnimation = false;
		}, 400);
	}

}
