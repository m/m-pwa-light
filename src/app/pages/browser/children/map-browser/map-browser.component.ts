import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
	templateUrl: './map-browser.component.html',
	styleUrls: ['./map-browser.component.scss']
})
export class MapBrowserComponent implements OnInit {

	maps: any[] = [];

	constructor(private activatedRoute: ActivatedRoute) { }

	ngOnInit(): void {
		this.maps = this.activatedRoute.snapshot.data.maps;
	}

}
