import { Component, OnInit, OnDestroy } from '@angular/core';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Favorite, FavoritesService, FavoriteLinesSelectorDialogService } from '@metromobilite/m-features/favorites';
import { LinesService } from '@metromobilite/m-features/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
	templateUrl: './favorites.component.html',
	styleUrls: ['./favorites.component.scss']
})
export class FavoritesComponent implements OnInit, OnDestroy {

	items: Favorite[] = [];
	reorder: boolean;

	private unsubscriber = new Subject();

	constructor(
		private favoritesService: FavoritesService,
		private favoriteLinesSelectorDialogService: FavoriteLinesSelectorDialogService,
		private linesService: LinesService
	) { }

	ngOnInit(): void {
		this.items = this.loadFavorites();
		this.favoritesService.changes$.pipe(takeUntil(this.unsubscriber)).subscribe(favorites => {
			this.items = this.loadFavorites();
		});
	}

	ngOnDestroy(): void {
		this.unsubscriber.next();
		this.unsubscriber.complete();
	}

	removeFavorite(favorite: Favorite) {
		this.favoritesService.remove(favorite);
	}

	editFavorite(favorite: Favorite) {
		this.favoriteLinesSelectorDialogService.open(favorite.poi).subscribe(response => {
			this.favoritesService.handleLineSelectorResponse(response, favorite.poi);
		});
	}

	removeAll() {
		this.favoritesService.removeAll();
	}

	reorderFav() {
		this.reorder = true;
	}

	cancelReorder() {
		this.reorder = false;
		this.items = this.loadFavorites();
	}

	confirmReorder() {
		this.favoritesService.setFavorites(this.items);
		this.favoritesService.save();
		this.reorder = false;
	}

	drop(event: CdkDragDrop<string[]>) {
		moveItemInArray(this.items, event.previousIndex, event.currentIndex);
	}

	private loadFavorites() {
		return this.favoritesService.getFavorites();
	}

}
