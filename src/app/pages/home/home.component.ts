import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { trigger, transition, useAnimation, query, style, stagger, animate } from '@angular/animations';
import { finalize, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { logoEnterAnimation, logoLeaveAnimation } from '@animations/logo.animation';
import { favoriteInAnimation, favoriteOutAnimation, Favorite, FavoritesService, FavoriteLinesSelectorDialogService } from '@metromobilite/m-features/favorites';
import { Poi } from '@metromobilite/m-features/reference';
import { LinesService, GeolocationService, AppVisibilityService } from '@metromobilite/m-features/core';
import { NearestStopsService } from '@metromobilite/m-features/nearest-stops';
import { SearchFavoritesDialogService } from '@features/favorites/search-favorites-dialog/search-favorites-dialog.service';

@Component({
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss'],
	animations: [
		trigger('listAnimation', [
			transition('* => *', [
				query('.mat-list-item', style({ opacity: 0, transform: 'translate(42%, 0)' }), { optional: true }),
				query(':enter', [
					stagger(75, [
						animate('220ms', style({ opacity: 1, transform: 'translate(0, 0)' }))
					]),
				], { optional: true }),
			])
		]),
		trigger('contentLoaderAnimation', [
			transition(':enter', [
				logoEnterAnimation()
			]),
			transition(':leave', [
				logoLeaveAnimation()
			])
		]),
		trigger('favoriteAnimation', [
			transition('false => true', [
				useAnimation(favoriteInAnimation, { params: { time: '220ms' } })
			]),
			transition('true => false', [
				useAnimation(favoriteOutAnimation, { params: { time: '220ms' } })
			]),
		]),
		trigger('fadeScale', [
			transition(':enter', [
				style({ opacity: 0, transform: 'scale(0)' }),
				animate(220, style({ opacity: 1, transform: '*' }))
			]),
			transition(':leave', [
				style({ opacity: 1, transform: 'scale(1)' }),
				animate(220, style({ opacity: 0, transform: 'scale(0)' }))
			]),
		]),
		trigger('fadeDown', [
			transition(':enter', [
				style({ opacity: 0, transform: 'translateY(50%)', position: 'absolute', width: '100%' }),
				animate(330, style({ opacity: 1, transform: 'translateY(0)' }))
			]),
			transition(':leave', [
				style({ opacity: 1, transform: 'translateY(0)', position: 'absolute', width: '100%' }),
				animate(330, style({ opacity: 0, transform: 'translateY(50%)' }))
			]),
		]),
		trigger('realtimeAnimation', [
			transition('* => visible', [
				query(':enter', [
					style({ opacity: 0 }),
					stagger(300, [
						animate(330, style({ opacity: '*' }))
					]),
				], { optional: true })
			]),
		]),
	]
})
export class HomeComponent implements OnInit, OnDestroy {

	favorites: Favorite[];
	nearestClusters: Poi[];
	refusedGeolocation: boolean;
	displayClustersLoader: boolean;
	dist: string;
	loadingClusters = false;

	private unsubscriber = new Subject<void>();

	constructor(
		private favoritesService: FavoritesService,
		private favoriteLinesSelectorDialogService: FavoriteLinesSelectorDialogService,
		private searchFavoritesDialogService: SearchFavoritesDialogService,
		private linesService: LinesService,
		private geolocationService: GeolocationService,
		private nearestStopsService: NearestStopsService,
		private appVisibilityService: AppVisibilityService,
		private activatedRoute: ActivatedRoute
	) {
		this.dist = this.nearestStopsService.dist;
	}

	ngOnInit(): void {
		this.favorites = this.loadFavorites();
		this.nearestClusters = this.activatedRoute.snapshot.data.nearestClusters || [];
		this.refusedGeolocation = this.geolocationService.refused;
		this.appVisibilityService.visibility$.pipe(takeUntil(this.unsubscriber)).subscribe(visible => {
			if (visible && this.refusedGeolocation) {
				this.refreshNearsetClusters();
			}
		});
		this.favoritesService.changes$.pipe(takeUntil(this.unsubscriber)).subscribe(favorites => {
			this.favorites = this.loadFavorites();
		});
	}

	ngOnDestroy(): void {
		this.unsubscriber.next();
		this.unsubscriber.complete();
	}

	removeFavorite(favorite: Favorite) {
		this.favoritesService.remove(favorite);
	}

	editFavorite(favorite: Favorite) {
		this.favoriteLinesSelectorDialogService.open(favorite.poi).subscribe(response => {
			this.favoritesService.handleLineSelectorResponse(response, favorite.poi);
		});
	}

	refreshNearsetClusters() {
		const timer = setTimeout(() => {
			this.displayClustersLoader = true;
		}, 500);
		this.nearestClusters = [];
		this.loadingClusters = true;
		this.geolocationService.getCurrentPosition().then((position: Position) => {
			this.nearestStopsService.getNearestStops(position.coords.latitude, position.coords.longitude)
				.pipe(finalize(() => {
					clearTimeout(timer);
					this.displayClustersLoader = false;
					this.loadingClusters = false;
				}))
				.subscribe(collection => {
					this.nearestClusters = collection;
				});
		});
	}

	searchFavorites() {
		this.searchFavoritesDialogService.open();
	}

	private loadFavorites() {
		return this.favoritesService.getFavorites();
	}

}
