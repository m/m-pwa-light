import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { trigger, transition, useAnimation, query, style, stagger, animate } from '@angular/animations';
import { favoriteSlideLeftAnimation, favoriteSlideRightAnimation } from '@metromobilite/m-features/favorites';
import { Line, LinesService, ClustersService } from '@metromobilite/m-features/core';

@Component({
	templateUrl: './line-detail.component.html',
	styleUrls: ['./line-detail.component.scss'],
	animations: [
		trigger('favoriteAnimation', [
			transition('false => true', [
				useAnimation(favoriteSlideLeftAnimation, { params: { time: '220ms' } })
			]),
			transition('true => false', [
				useAnimation(favoriteSlideRightAnimation, { params: { time: '220ms' } })
			]),
		]),
		trigger('realtimeAnimation', [
			transition('* => visible', [
				query(':enter', [
					style({ opacity: 0 }),
					stagger(300, [
						animate(330, style({ opacity: 1 }))
					]),
				], { optional: true })
			]),
		])
	]
})
export class LineDetailComponent implements OnInit {

	line: Line;
	stopsFilter: string;
	clusters: any[];
	multiLineTitle: string[];
	ghosts = Array(2);

	constructor(
		private activatedRoute: ActivatedRoute,
		private linesService: LinesService,
		private clustersService: ClustersService
	) { }

	ngOnInit(): void {
		this.line = this.linesService.find(this.activatedRoute.snapshot.params.id);
		this.multiLineTitle = this.clustersService.splitLongName(this.line.longName);
		this.clusters = this.activatedRoute.snapshot.data.clusters;
	}

}
