import { Component, OnInit, OnDestroy } from '@angular/core';
import { trigger, style, transition, query, animate, stagger, useAnimation } from '@angular/animations';
import { ActivatedRoute } from '@angular/router';
import { finalize, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { logoEnterAnimation, logoLeaveAnimation } from '@animations/logo.animation';
import { favoriteInAnimation, favoriteOutAnimation } from '@metromobilite/m-features/favorites';
import { Poi } from '@metromobilite/m-features/reference';
import { NearestStopsService } from '@metromobilite/m-features/nearest-stops';
import { GeolocationService, AppVisibilityService } from '@metromobilite/m-features/core';

@Component({
	templateUrl: './nearest-stops.component.html',
	styleUrls: ['./nearest-stops.component.scss'],
	animations: [
		trigger('listAnimation', [
			transition('* => *', [
				query('.mat-list-item', style({ opacity: 0, transform: 'translate(42%, 0)' }), { optional: true }),
				query(':enter', [
					stagger(75, [
						animate('220ms', style({ opacity: 1, transform: 'translate(0, 0)' }))
					]),
				], { optional: true }),
			])
		]),
		trigger('contentLoaderAnimation', [
			transition(':enter', [
				logoEnterAnimation()
			]),
			transition(':leave', [
				logoLeaveAnimation()
			])
		]),
		trigger('favoriteAnimation', [
			transition('false => true', [
				useAnimation(favoriteInAnimation, { params: { time: '220ms' } })
			]),
			transition('true => false', [
				useAnimation(favoriteOutAnimation, { params: { time: '220ms' } })
			]),
		])
	]
})
export class NearestStopsComponent implements OnInit, OnDestroy {

	displayLoader = false;
	loading = false;
	clusters: Poi[] = [];
	dist: string;

	private unsubscriber = new Subject<void>();

	constructor(
		private activatedRoute: ActivatedRoute,
		private nearestStopsService: NearestStopsService,
		private geolocationService: GeolocationService,
		private appVisibilityService: AppVisibilityService
	) {
		this.dist = this.nearestStopsService.dist;
	}

	ngOnInit(): void {
		this.clusters = this.activatedRoute.snapshot.data.clusters;
		this.appVisibilityService.visibility$.pipe(takeUntil(this.unsubscriber)).subscribe(visible => {
			if (visible) {
				this.refresh();
			}
		});
	}

	ngOnDestroy(): void {
		this.unsubscriber.next();
		this.unsubscriber.complete();
	}

	refresh() {
		const timer = setTimeout(() => {
			this.displayLoader = true;
		}, 500);
		this.clusters = [];
		this.loading = true;
		this.geolocationService.getCurrentPosition().then((position: Position) => {
			this.nearestStopsService.getNearestStops(position.coords.latitude, position.coords.longitude)
				.pipe(finalize(() => {
					clearTimeout(timer);
					this.displayLoader = false;
					this.loading = false;
				}))
				.subscribe(collection => {
					this.clusters = collection;
				});
		});
	}

}
