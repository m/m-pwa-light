import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { trigger, transition, query, style, animate, stagger, useAnimation, group, animateChild } from '@angular/animations';
import { favoriteSlideLeftAnimation, favoriteSlideRightAnimation, FavoritesService, FavoriteLinesSelectorDialogService } from '@metromobilite/m-features/favorites';
import { Poi } from '@metromobilite/m-features/reference';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';

export const REALTIME_STRATEGY_KEY = 'app:realtime:strategy';

@Component({
	templateUrl: './stops-zone.component.html',
	styleUrls: ['./stops-zone.component.scss'],
	animations: [
		trigger('realtimeAnimation', [
			transition('* => visible', [
				query(':enter', [
					style({ opacity: 0 }),
					stagger(300, [
						animate(330, style({ opacity: 1 }))
					]),
				], { optional: true })
			]),
		]),
		trigger('favoriteAnimation', [
			transition('false => true', [
				useAnimation(favoriteSlideLeftAnimation, { params: { time: '220ms' } })
			]),
			transition('true => false', [
				useAnimation(favoriteSlideRightAnimation, { params: { time: '220ms' } })
			]),
		]),
		trigger('itemsAnimation', [
			transition('init <=> *', [
				query(':enter', [
					query('mf-logo-ligne, .m-multiline, .time-wrapper', [
						style({ opacity: 0 }),
						stagger(300, animate(330, style({ opacity: 1 })))
					], { optional: true }),
				], { optional: true }),
			]),
			transition(':increment, :decrement', [
				query(':leave', [
					style({ opacity: 1, transform: 'translateX(0)' }),
					stagger(450, animate(450, style({ opacity: 0, transform: 'translateX(-50%)', height: 0, margin: '0' })))
				], { optional: true }),
				query(':enter', [
					style({ opacity: 0, transform: 'translateX(50%)', height: 0, margin: '0' }),
					stagger(450, animate(450, style({ opacity: 1, transform: 'translateX(0)', height: '*', margin: '*' })))
				], { optional: true }),
			])
		]),
	]
})
export class StopsZoneComponent implements OnInit, OnDestroy {
	poi: Poi;
	isFavorite: boolean;
	strategy = localStorage.getItem(REALTIME_STRATEGY_KEY) || 'per-lines';
	ghosts = Array(10);
	enableStopZoneSortFeature = environment.enableStopZoneSortFeature;

	private unsubscriber = new Subject();

	constructor(
		private activatedRoute: ActivatedRoute,
		private favoritesService: FavoritesService,
		private favoriteLinesSelectorDialogService: FavoriteLinesSelectorDialogService
	) { }

	ngOnInit(): void {
		this.poi = this.activatedRoute.snapshot.data.poi;
		this.isFavorite = this.favoritesService.isFavorite(this.poi);
		this.favoritesService.changes$.pipe(takeUntil(this.unsubscriber)).subscribe(() => {
			this.isFavorite = this.favoritesService.isFavorite(this.poi);
		});
	}

	ngOnDestroy(): void {
		this.unsubscriber.next();
		this.unsubscriber.complete();
	}

	editFavorite() {
		this.favoriteLinesSelectorDialogService.open(this.poi).subscribe((response) => {
			this.favoritesService.handleLineSelectorResponse(response, this.poi);
			this.isFavorite = this.favoritesService.isFavorite(this.poi);
		});
	}

	toggleStrategy() {
		this.strategy = this.strategy === 'chrono' ? 'per-lines' : 'chrono';
		localStorage.setItem(REALTIME_STRATEGY_KEY, this.strategy);
	}

}
