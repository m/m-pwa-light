export const environment = {
	production: true,
	api: 'prod',
	matomo: true,
	matomoConfig: {
		url: '//www.mobilites-m.fr/stats/',
		id: 17,
		events: {
			navigation: true,
			deepLink: true,
			'stop--expand': false,
			timesheet: true,
		}
	},
	enableStopZoneSortFeature: true,
};
